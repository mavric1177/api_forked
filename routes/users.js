const express = require('express');
const router = express.Router({});


/* GET users listing. */
router.get('/login', function (req, res) {
    /* res.json({
    //     'status': 'OK'
  });*/
    console.log(req.body.first_name);
    var newUser = {
      first_name: req.body.first_name,
      last_name: req.body.last_name,
    }
    res.status(201).send(newUser);
});

router.get('/register', function (req, res) {
    res.json({
        'status': 'OK'
    });
    console.log(res);
});

router.post('/', function (req, res) {
    res.status(201).json(req.body);
});

router.put('/', function (req, res) {
    res.status(202).send();
});

router.delete('/', function (req, res) {
    res.status(202).send();
    process.exit();
});

router.patch('/', function (req, res) {
    res.status(202).json(req.body);
});

router.options('/', function (req, res) {
    res.header('Allow', 'GET, POST, PUT, PATCH, DELETE, OPTIONS, HEAD').status(204).send();
});

module.exports = router;
