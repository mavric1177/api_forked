const express = require('express');
const router = express.Router({});
const mongoose = require('mongoose');
mongoose.connect('mongo://localhost/gateway', { useNewUrlParser: true });

let db = mongoose.connection;

/*error handling
  if db does not open, log in console*/
db.once('open', function callback() {
    console.log("Connected to database");
});

db.on('error', function(err){
  console.log("error, counld not connect");
});

const Schema = mongoose.Schema;

var heartbeatSchema = new Schema ({
  gatewayId: {type: String, required: true},
  timestamp: {type: String, required: true}
},{collection: 'gateway'});

/*heartbeat model*/
var Heartbeat = mongoose.model('Heartbeat', heartbeatSchema);

/*heartbeat = {'gatewayId': String,
                'timestamp': String}; */
/*return a heartbeat model from the database*/
router.get('/getHeartbeat', function (req,res) {

  Heartbeat.find()
    .then(function(doc) {
      res.send({items: doc});
  });
});

/*heartbeat = {'gatewayId': String,
                'timestamp': String}; */
/*insert a heartbeat model into the database*/
router.post('/insertHeartbeat', function (req,res) {

  var data = {
    'gatewayId':  req.body.gatewayId,
    'timestamp': req.body.timepstamp
  };

  var heartbeat = new Heartbeat(data);
  heartbeat.save();

  res.status(201).json(heartbeat);
});


router.post('/registerGateway', function (req,res) {
    res.json({
      'status': 'OK'
    });
    console.log(res);
});

router.post('/authorizeGateway', function (req,res){
    res.json({
      'status': 'OK'
    });
    console.log(res);
});

router.get('/usersAuthorized', function(req,res){
    res.json({
      'status': 'OK'
    });
    console.log(res);
});


router.options('/', function (req, res) {
    res.header('Allow', 'GET, POST, PUT, PATCH, DELETE, OPTIONS, HEAD').status(204).send();
});

module.exports = router;
